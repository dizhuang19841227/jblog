<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.newflypig.jblog.model.Category" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/resources/css/common.css">
	<title>分类列表</title>
</head>
<body>
	<a href="add">添加分类</a>
	<%for(Category c : (List<Category>)request.getAttribute("categories")) {%>
		<h1><%=c.getTitle() %>&nbsp;<%=c.getNumber() %>---<a href="<%=c.getCategoryId()%>/delete">删除</a>
		---<a href="<%=c.getCategoryId()%>/show">查看</a>
		---<a href="<%=c.getCategoryId()%>/update">修改</a></h1>
	<%}%>
</body>
</html>