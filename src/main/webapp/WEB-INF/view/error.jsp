<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.newflypig.jblog.exception.LoginException"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/common.css">
<title>Insert title here</title>
</head>
<body style="text-align: center;padding-top: 200px">
	<h1>Oppps,There's something wrong happend!</h1>
	<h2>${exception.message}</h2>
	<%if(request.getAttribute("exception") instanceof LoginException){%>
		<a href="<%=request.getContextPath()%>/login">返回</a>
	<%}else{%>
		<a href="/<%=request.getContextPath() %>">返回</a>
	<%}%>
</body>
</html>