package com.newflypig.jblog.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.newflypig.jblog.dao.CategoryDAO;
import com.newflypig.jblog.model.Category;

@Repository("categoryDao")
public class CategoryDAOImpl extends BaseHibernateDAO<Category> implements CategoryDAO{
	// property constants
	public static final String TITLE = "title";
	public static final String NUMBER = "number";
	public static final String TYPE = "type";
	
	@Override
	public void deleteById(Integer id){
		try{
			Query query=this.getSession().createQuery("delete from Category c where c.categoryId=?");
			query.setInteger(0, id);
			query.executeUpdate();
			log.info("Delete Category objects which id="+id+" successful");
		}catch(RuntimeException re){
			log.error("Occur an Error when Delete Category objects id="+id+"/n"+re);
			throw re;
		}
	}
}