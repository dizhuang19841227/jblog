package com.newflypig.jblog.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.newflypig.jblog.io.IOperations;

public interface BaseDAO<T extends Serializable> extends IOperations<T> {
	/**
	 * @param dc:查询条件，page:页码
	 * @return 分页数据列表
	 */
	public List<T> findPager(DetachedCriteria dc,int page);
	
	/**
	 * 根据某种条件，查找所有符合条件的个数
	 * @param dc
	 * @return 符合条件的个数
	 */
	public long getCount(DetachedCriteria dc);
}
