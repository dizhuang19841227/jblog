package com.newflypig.jblog.util;

import javax.annotation.Resource;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.newflypig.jblog.model.System;
import com.newflypig.jblog.model.SystemStatic;
import com.newflypig.jblog.service.SystemService;

public class InitServer implements ApplicationListener<ContextRefreshedEvent> {
	@Resource(name="systemService")
	private SystemService systemService;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		//root application context 没有parent，他就是老大.
		if(event.getApplicationContext().getParent() == null){
			//需要执行的逻辑代码，当spring容器初始化完成后就会执行该方法。
			System s=systemService.getSystem();
			SystemStatic.BLOG_TITLE=s.getBlogTitle();
			SystemStatic.BLOG_METAS=s.getBlogMetas();
			SystemStatic.BLOG_SCRIPTS=s.getBlogScripts();
			SystemStatic.BLOG_RECORD=s.getBlogRecord();
	    }
	}
}
