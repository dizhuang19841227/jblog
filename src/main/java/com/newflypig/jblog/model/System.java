package com.newflypig.jblog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * System entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "system", catalog = "jblog")
public class System implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -2839886268016617303L;
	
	private int	systemId;
	private String blogTitle;
	private String blogTheme;
	private String userName;
	private String password;
	private String introduction;
	private String blogMetas;
	private String blogScripts;
	private String blogRecord;
	
	@Id
	@GeneratedValue
	@Column(name = "system_id", unique = true, nullable = false)
	public int getSystemId() {
		return systemId;
	}
	public void setSystemId(int systemId) {
		this.systemId = systemId;
	}
	
	@Column(name="blog_title")
	public String getBlogTitle() {
		return blogTitle;
	}
	public void setBlogTitle(String blogTitle) {
		this.blogTitle = blogTitle;
	}
	
	@Column(name="blog_theme")
	public String getBlogTheme() {
		return blogTheme;
	}
	public void setBlogTheme(String blogTheme) {
		this.blogTheme = blogTheme;
	}
	
	@Column(name="username")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="introduction")
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	
	@Column(name="blog_metas")
	public String getBlogMetas() {
		return blogMetas;
	}
	public void setBlogMetas(String blogMetas) {
		this.blogMetas = blogMetas;
	}
	
	@Column(name="blog_scripts")
	public String getBlogScripts() {
		return blogScripts;
	}
	public void setBlogScripts(String blogScripts) {
		this.blogScripts = blogScripts;
	}
	
	@Column(name="blog_record")
	public String getBlogRecord() {
		return blogRecord;
	}
	public void setBlogRecord(String blogRecord) {
		this.blogRecord = blogRecord;
	}
}